package resolution.example6.zzeulki.practice94;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;

public class MainActivity extends Activity {

    private MediaPlayer mPlayer = null;
    private SurfaceView mView;
    private SurfaceHolder mHolder;
    private String mSdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String mVideoFile = "video.mp4";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mView = (SurfaceView) findViewById(R.id.surfaceview1);
        mHolder = mView.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mPlayer = new MediaPlayer();

        //mPlayer.setDisplay(mHolder);
    }

    protected void onDestroy(){
        if(mPlayer != null){
            mPlayer.release();
            mPlayer = null;
        }
        super.onDestroy();
    }

    public void startMP(View v){
        askpermission(android.Manifest.permission.READ_EXTERNAL_STORAGE);

        try{
            mPlayer.setDataSource(mSdPath+"/"+mVideoFile);
            mPlayer.setDisplay(mHolder);
            mPlayer.prepare();
        }catch (IOException e){
            e.printStackTrace();
            return;
        }

        mPlayer.setLooping(true);
        mPlayer.start();
    }

    public void stopMP(View v){
        mPlayer.stop();
        mPlayer.reset();
    }

    private void askpermission(String permission){
        if(ContextCompat.checkSelfPermission(MainActivity.this,permission)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,permission)){
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{permission},2);
            }else{
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{permission},2);
            }
        }

    }
}
